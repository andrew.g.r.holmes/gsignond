/*
 * This file is part of gsignond
 *
 * Copyright (C) 2012 Intel Corporation.
 *
 * Contact: Imran Zaman <imran.zaman@linux.intel.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef __GSIGNOND_DB_CREDENTIALS_DATABASE_H__
#define __GSIGNOND_DB_CREDENTIALS_DATABASE_H__

#include <glib.h>
#include <glib-object.h>
#include <gsignond.h>
#include "gsignond-identity-info.h"

#include "gsignond-db-metadata-database.h"

G_BEGIN_DECLS

/*
 * Type macros.
 */
#define GSIGNOND_DB_TYPE_CREDENTIALS_DATABASE gsignond_db_credentials_database_get_type ()
G_DECLARE_FINAL_TYPE (GSignondDbCredentialsDatabase, gsignond_db_credentials_database, GSIGNOND_DB, CREDENTIALS_DATABASE, GObject)

GSignondDbCredentialsDatabase*
gsignond_db_credentials_database_new (
        GSignondConfig *config,
        GSignondSecretStorage *storage);

gboolean
gsignond_db_credentials_database_open_secret_storage (
        GSignondDbCredentialsDatabase *self);

gboolean
gsignond_db_credentials_database_close_secret_storage (
        GSignondDbCredentialsDatabase *self);

gboolean
gsignond_db_credentials_database_is_open_secret_storage (
        GSignondDbCredentialsDatabase *self);

gboolean
gsignond_db_credentials_database_clear (
        GSignondDbCredentialsDatabase *self);

GSignondIdentityInfo *
gsignond_db_credentials_database_load_identity (
        GSignondDbCredentialsDatabase *self,
        const guint32 identity_id,
        gboolean query_secret);

GSignondIdentityInfoList *
gsignond_db_credentials_database_load_identities (
        GSignondDbCredentialsDatabase *self,
        GSignondDictionary *filter);

guint32
gsignond_db_credentials_database_update_identity (
        GSignondDbCredentialsDatabase *self,
        GSignondIdentityInfo* identity);

gboolean
gsignond_db_credentials_database_remove_identity (
        GSignondDbCredentialsDatabase *self,
        const guint32 identity_id);

gboolean
gsignond_db_credentials_database_check_secret (
        GSignondDbCredentialsDatabase *self,
        const guint32 identity_id,
        const gchar *username,
        const gchar *secret);

GSignondDictionary*
gsignond_db_credentials_database_load_data (
        GSignondDbCredentialsDatabase *self,
        const guint32 identity_id,
        const gchar *method);

gboolean
gsignond_db_credentials_database_update_data (
        GSignondDbCredentialsDatabase *self,
        const guint32 identity_id,
        const gchar *method,
        GSignondDictionary *data);

gboolean
gsignond_db_credentials_database_remove_data (
        GSignondDbCredentialsDatabase *self,
        const guint32 identity_id,
        const gchar *method);

GList *
gsignond_db_credentials_database_get_methods (
        GSignondDbCredentialsDatabase *self,
        const guint32 identity_id,
        GSignondSecurityContext* sec_ctx);

gboolean
gsignond_db_credentials_database_insert_reference (
        GSignondDbCredentialsDatabase *self,
        const guint32 identity_id,
        const GSignondSecurityContext *ref_owner,
        const gchar *reference);
gboolean
gsignond_db_credentials_database_remove_reference (
        GSignondDbCredentialsDatabase *self,
        const guint32 id,
        const GSignondSecurityContext *ref_owner,
        const gchar *reference);

GList *
gsignond_db_credentials_database_get_references (
        GSignondDbCredentialsDatabase *self,
        const guint32 id,
        const GSignondSecurityContext *ref_owner);

GList *
gsignond_db_credentials_database_get_accesscontrol_list(
        GSignondDbCredentialsDatabase *self,
        const guint32 identity_id);

GSignondSecurityContext *
gsignond_db_credentials_database_get_owner(
        GSignondDbCredentialsDatabase *self,
        const guint32 identity_id);

GSignondSecurityContext *
gsignond_db_credentials_database_get_identity_owner (
        GSignondDbCredentialsDatabase *self,
        const guint32 identity_id);

const GError *
gsignond_db_credentials_database_get_last_error (
        GSignondDbCredentialsDatabase *self);

G_END_DECLS

#endif /* __GSIGNOND_DB_CREDENTIALS_DATABASE_H__ */

